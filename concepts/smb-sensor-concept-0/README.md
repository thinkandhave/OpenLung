# CONCEPT:
---
## Scope:
- Hardware including sensors connected to controller, ventilator, and humidifier, not including those hardware components themselves

## Team Lead(s):
|GitLab|Slack|
|---|---|
| | |
| | |

## Slack Channels:
-

## Issue Labels:
- Sensor Hardware

## Current Collab Docs:

## Overview:
