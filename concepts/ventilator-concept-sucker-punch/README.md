Features:
-	All custom parts are either sheet metal stampings or rods and tubes cut to length.
-	All rods and tubes are the same diameter
-	Design is setup to be equally well made as stampings, laser cuts or CNC milled parts 
-	Most parts are orientation agnostic
-	Only one part needs folding. No large radii. No internal folds.
-	Easily available skate board roller
-	Widely available 608 Ball bearings
-	Minimal friction on the Ambubag
-	The cam track is replaceable when worn out
-	A ball bearing reduces wear on the cam track
-	Adjustable pump volume (Still needs some work) 
-	Very good access to the Ambubag
-	Easily taken apart and disinfected
-	12V gear motor
-	The separate motor mounting plate can be easily customized to the mounting wholes of the used motor
-	Most of the motor weight rests on the ground and not on the housing

-	Opposed to concept 8 the whole pump volume can be used
-	No risk of pulling the bag into the machine if the roller locks up
-	Due to the cam track mechanism the pump volume rises nonlinearly. Much like natural breathing. But I don’t know if this makes a difference.

Issues to be addressed: 
-	The air volume adjustment is not yet bullet proof
-	0 points for looks. Everything is geared towards cost and ease of manufacture.
-	Little space for the driver circuit